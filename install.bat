@echo off
setlocal EnableDelayedExpansion

set NAME=𝘈𝘱𝘱𝘕𝘢𝘮𝘦
set VERSION=1.0.0


echo Installation of %NAME% %VERSION% started. Please wait.

set destination=%LocalAppData%\Programs\%NAME%
rd /s /q "%destination%"
powershell Expand-Archive src.zip -DestinationPath "%destination%" -Force
xcopy /sy uninstall.bat %destination%

set local_path=
for /f "skip=2 tokens=3*" %%a in ('reg query HKCU\Environment /v PATH') do if [%%b]==[] (set local_path="%%~a") else (set local_path="%%~a;%%~b")
set "local_path=!local_path:%destination%=!"
set "local_path=%local_path%;%destination%"
setx PATH "%local_path%"

cls
echo Installation of %NAME% %VERSION% completed.
timeout 5 > NUL