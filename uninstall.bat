@echo off
setlocal EnableDelayedExpansion

set NAME=𝘈𝘱𝘱𝘕𝘢𝘮𝘦


set destination=%LocalAppData%\Programs\%NAME%
rd /s /q "%destination%"

set local_path=
for /f "skip=2 tokens=3*" %%a in ('reg query HKCU\Environment /v PATH') do if [%%b]==[] (set local_path="%%~a") else (set local_path="%%~a;%%~b")
set "local_path=!local_path:%destination%=!"
setx PATH "%local_path%"

cls
echo %NAME% uninstalled.
timeout 5 > NUL