# Batch Installer

This is a simple but useful installer for windows that helps you share your [cli software](https://en.wikipedia.org/wiki/Command-line_interface) without worries about false-positive alerts.

# How to create the installer

To create the installer for your software follow these steps:

1. Edit the `NAME` and `VERSION` constants in the `install.bat` and `uninstall.bat` files so they match with your software (use CamelCase).
1. Compress your software in a file named `src.zip`.
1. Compress again the `install.bat`, `uninstall.bat` and `src.zip` files to a zip with a name of your choice (eg: **myapp-1.0.0-win64.zip**).

That's it! Now just upload it and share the link.  

For example: https://www.acitd.com/dist/myapp-1.0.0-win64.zip

# How to use the installer

Once the users downloaded your file (eg: **myapp-1.0.0-win64.zip**) they have to:

1. Unzip it.
2. Double-click to the `install.bat`.

Now the users will be able to open the a new cmd and access directly to the binary files.

### For example:

Let's say you have a `app.exe` in your software directory and you run it like this:

```
C:\…\myapp-direcotry\app.exe --version
```
After the installation, every time they open the cmd they will be able to run it like this:
```
app --version
```

# Support

This software is made by [**Alex Costantino**](https://www.acitd.com/en/).

If you like my work, please consider making a [**donation**](https://revolut.me/alexcostantino).  
This will **allow me to improve** the existing projects and **develop new** ones that will make **your life easier**.

You can also support me by choosing my [**services**](https://www.acitd.com/en/services) such as:

- Managed Hosting
- IT Support
- SW Development
- UI/UX & Graphic Design

You can send me money [**HERE**](https://revolut.me/alexcostantino).  
In the **Note** section you can refer your info, suggest improvements or write whatever you want.

Email: alex@acitd.com